import os
from lxml import etree, objectify
from parser import parse
from datetime import datetime
from dateutil.parser import parse
from dateutil.parser._parser import ParserError
import calendar
import json
import re
from distutils.util import strtobool
from collections import defaultdict

from mongoengine import *


from config import format_transformation, fall_back_ns
from ar_class import Filing, Annual_Report, meta_data
from make_python_identifier import make_python_identifier

MONGODB_USERNAME='user'
MONGODB_PASSWORD='pass'
MONGODB_HOSTNAME='mongodb'
MONGODB_DATABASE='ch_files'

ar_directory = '../annual_reports'

def check_pythonic(field):
    if field.isidentifier():
        pass
    else:
        field, _ = make_python_identifier(field)
    return field


if __name__ == "__main__":
    connect(db='ch_files', username='user', password='pass', host='mongodb://user:pass@mongo:27017/ch_files', connect=False)

    file_names = os.listdir(ar_directory)

# xml files = [270,760,867,1048,1166,1788,2041,2137,2245,2295,2917,3282,3539,3722,4015,4177,4539,4589,4766,5615,6043,6286,6344,6560,7011,7181,7382]


    for file_name in file_names:
        #file_name = file_names[0]

        extract = file_name.split("_")
        ch_number = extract[2]
        year_e = datetime.strptime(re.match(r'^(.*?)\..*',extract[3])[1],"%Y%m%d")

        filing = Filing(ch_number = ch_number, year_e = year_e)

        loc_file_name = os.path.abspath("{}/{}".format(ar_directory,file_name))
        
        with open(loc_file_name,'rb') as f:
            ch_file = f.read()

        file_type = file_name.split('.')[-1]

        
        tree = etree.XML(ch_file)
        ns_file = tree.nsmap
        if file_type == 'html':
            xbrl_key = 'ix'
            xbrl_url = ns_file[xbrl_key]
            for el in tree.iter(f"{{{xbrl_url}}}*"):
                attribute = dict(el.attrib)

                if attribute != {}:

                    if el.text != None:
                        if el.text.strip().rstrip()!="":
                            if 'name' in attribute:
                                id, name_tag = attribute['name'].split(':')
                                name_tag = check_pythonic(name_tag)
                                try:
                                    annual_report = Annual_Report(**meta_data[name_tag])
                                except KeyError:
                                    annual_report = Annual_Report()
                            if 'format' in attribute:
                                transform = attribute['format'].split(':')
                                format_ = format_transformation[ns_file[transform[0]]][transform[1]]["type"]
                                annual_report.format = format_
                                annual_report.taxonomy = xbrl_url
                            text = el.text.strip().rstrip()
                            annual_report.value = str(text)
                            annual_report.id = str(id)
                            if 'decimal' in attribute:
                                annual_report.decimal = int(attribute['decimal'])
                            if 'context_ref' in attribute:
                                annual_report.context_ref = attribute['context_ref']
    
                            filing[name_tag] = annual_report
                                                    
        elif file_type == 'xml':
            for ns in ns_file:
                for el in tree.iter("{"+ns_file[ns]+"}*"):
                    attribute = dict(el.attrib)
                    if el.text != None:
                        if el.text.strip().rstrip()!="":
                            name_tag = etree.QName(el).localname
                            name_tag = check_pythonic(name_tag)
                            try:
                                annual_report = Annual_Report(**meta_data[name_tag])
                            except KeyError:
                                annual_report = Annual_Report()
                            if 'contextRef' in attribute:
                                annual_report.context_ref = attribute['contextRef']
                            annual_report.value = str(format(el.text.strip().rstrip()))
                            if 'decimal' in attribute:
                                annual_report.decimal = attribute['decimal']
                        
                            filing[name_tag]=annual_report
        # print(filing.to_json())            
        #filing.save()
        filing.validate()