// var db = connect("mongodb://admin:pass@localhost:27017/admin");

print('############################################################')

conn = new Mongo();

db = db.getSiblingDB('ch_files'); // we can not use "use" statement here to switch db

db.createUser(
    {
        user: "user",
        pwd: "pass",
        roles: [ { role: "readWrite", db: "ch_files"} ],
    }
);
db.createCollection('ch_filing');