import lxml
import os
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import pandas as pd
import os
from lxml import etree
import datetime
from html.parser import HTMLParser
from html.entities import name2codepoint
import re

def xbrl_name(html_file):
    '''Reads the xbrl names used in this html'''
    record_name = []
    tree=ET.parse(html_file)
    root = tree.getroot()
    for record in [elem.attrib for elem in root.iter()]:
        try:
            record_name.append(record['name'])
        except KeyError as error:
            continue
    return record_name

def xbrl_download(html_file,xbrl_names):
    '''Captures the data in the html file'''
    tree=ET.parse(html_file)
    root=tree.getroot(tree)
    t=tostring(root)
    tree_string=ET.fromstring(t)
    soup = BeautifulSoup(t,'lxml')
    record_key = []
    record_value = []
    for record in xbrl_names:
        ch_data=soup.find(attrs={'name':record}).text
        record_key.append(record)
        record_value.append(ch_data)
    CH_data = pd.DataFrame({'xbrl_tag':record_key,'ch_data':record_value})
    return CH_data

class _HTMLToText(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self._buf = []
        self.hide_output = False

    def handle_starttag(self, tag, attrs):
        if tag in ('p', 'br') and not self.hide_output:
            self._buf.append('\n')
        elif tag in ('script', 'style'):
            self.hide_output = True

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self._buf.append('\n')

    def handle_endtag(self, tag):
        if tag == 'p':
            self._buf.append('\n')
        elif tag in ('script', 'style'):
            self.hide_output = False

    def handle_data(self, text):
        if text and not self.hide_output:
            self._buf.append(re.sub(r'\s+', ' ', text))

    def handle_entityref(self, name):
        if name in name2codepoint and not self.hide_output:
            c = unichr(name2codepoint[name])
            self._buf.append(c)

    def handle_charref(self, name):
        if not self.hide_output:
            n = int(name[1:], 16) if name.startswith('x') else int(name)
            self._buf.append(unichr(n))

    def get_text(self):
        return re.sub(r' +', ' ', ''.join(self._buf))

def html_to_text(html):
    """
    Given a piece of HTML, return the plain text it contains.
    This handles entities and char refs, but not javascript and stylesheets.
    """
    parser = _HTMLToText()
    parser.feed(html)
    parser.close()
    return parser.get_text()

def get_tree(name):
    file_name = "./annual_reports/{}".format(name)
    with open(file_name,'rb') as f:
        ch_file = f.read()
    tree = etree.XML(ch_file)
    return tree