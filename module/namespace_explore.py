import os
from lxml import etree, objectify
import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt

def get_population_nsmap(file_names):
    list_nsmaps= []
    for name in file_names:
        tree = get_tree(name)
        list_nsmaps.append(tree.nsmap)
    return list_nsmaps


def get_tree(name):
    file_name = "./annual_reports/{}".format(name)
    with open(file_name,'rb') as f:
        ch_file = f.read()
    tree = etree.XML(ch_file)
    return tree


def get_ns_keys(pop_nsmap):
    ns_keys_ = set()
    for ns_map in pop_nsmap:
        ns_keys_.update(list(ns_map.keys()))
    return list(ns_keys_)

if __name__ == "__main__":
    file_names = os.listdir('./annual_reports')
    population_nsmap = get_population_nsmap(file_names)

    ns_keys = get_ns_keys(population_nsmap)

    key_taxonomy = {}
    for key_ in ns_keys:
        taxonomies = set()
        for ns_map in population_nsmap:
            try:
                taxonomy = ns_map[key_]
                taxonomies.add(taxonomy)
            except KeyError:
                None
        key_taxonomy[key_]=list(taxonomies)

    print(f" key_taxonomy: \n{key_taxonomy}")

    ### The 'ix' is the key to use for pulling out the inline xbrl

    ## What is the distribution of namespaces?

    ns_length = []
    for ns_map in population_nsmap:
        ns_length.append(len(ns_map))
    pd.Series({'namespace size':ns_length}).hist(bins=40)
    plt.show()


    ## What's the most common namespace?

    cnt = Counter()
    for ns_map in population_nsmap:
        for key_ in list(ns_map.keys()):
            cnt[key_]+=1
    counter = dict(cnt)
    print(sorted(counter.items(), key=lambda x: x[1], reverse=True))
