import os
from lxml import etree, objectify
from parser import parse
from datetime import datetime
from dateutil.parser import parse
from dateutil.parser._parser import ParserError
import calendar
import json
import re
from distutils.util import strtobool
from collections import defaultdict

from mongoengine import *


from config import format_transformation, fall_back_ns


MONGODB_USERNAME='user'
MONGODB_PASSWORD='pass'
MONGODB_HOSTNAME='mongodb'
MONGODB_DATABASE='ch_files'

def multi_level_dict():
    """ Constructor for creating multi-level nested dictionary. """

    return defaultdict(multi_level_dict)

def infer_type(text):
    try:
        data = float(text)
    except ValueError:
        try:
            data = parse(text)
        except (ParserError, calendar.IllegalMonthError, TypeError):
            try:
                data = strtobool(text)
                data = bool(data)
            except ValueError:
                data = str(text)
    return data

def replace_url(url):
    return url.replace(".","_")

# does the equivalent constructor for DynamicDocument work too?

class Filing(DynamicDocument):
    ch_number = StringField()
    year_e = DateTimeField()

    meta = {'collection': 'ch_filing'}

ar_directory = '../annual_reports'

if __name__ == "__main__":
    connect(db='ch_files', username='user', password='pass', host='mongodb://user:pass@mongo:27017/ch_files', connect=False)

    file_names = os.listdir(ar_directory)

# xml files = [270,760,867,1048,1166,1788,2041,2137,2245,2295,2917,3282,3539,3722,4015,4177,4539,4589,4766,5615,6043,6286,6344,6560,7011,7181,7382]


    for file_name in file_names:
        #file_name = file_names[270]

        extract = file_name.split("_")
        ch_number = extract[2]
        year_e = datetime.strptime(re.match(r'^(.*?)\..*',extract[3])[1],"%Y%m%d")

        loc_file_name = os.path.abspath("{}/{}".format(ar_directory,file_name))
        
        with open(loc_file_name,'rb') as f:
            ch_file = f.read()

        file_type = file_name.split('.')[-1]

        filing_dict = multi_level_dict()
        tree = etree.XML(ch_file)
        ns_file = tree.nsmap
        if file_type == 'html':
            xbrl_key = 'ix'
            xbrl_url = ns_file[xbrl_key]
            for el in tree.iter(f"{{{xbrl_url}}}*"):
                attribute = dict(el.attrib)

                if attribute != {}:
                    try:
                        context_ref = attribute['contextRef']
                    except KeyError:
                        pass

                    if 'name' in attribute:
                        id, name_tag = attribute['name'].split(':')

                        try:
                            name_taxonomy = ns_file[id]
                        except KeyError:
                            name_taxonomy = fall_back_ns[id]
                    format_ = False
                    if 'format' in attribute:
                        transform = attribute['format'].split(':')
                        format_ = format_transformation[ns_file[transform[0]]][transform[1]]["type"]
                    if el.text != None:
                        if el.text.strip().rstrip()!="":
                            text = el.text.strip().rstrip()
                            if format_:
                                if format_ == "date":
                                    data = parse(text)
                                elif format_ == "float":
                                    decimal = attribute['decimals']
                                    if isinstance(decimal, str):
                                        data = int(float(text.replace(",","")))
                                    else:
                                        decimal = int(decimal)
                                        if decimal >0:
                                            data = round(float(text.replace(",","")), decimal)
                                        else:
                                            data = int(float(text.replace(",","")))
                                elif format_ == "dashzero":
                                    data = int(text.replace("-","0"))
                                elif format_ == "boolean":
                                    data = format_transformation[ns_file[transform[0]]][transform[1]]["output"]
                                    data = bool(data)
                            else:
                                data = infer_type(text)
                            

                            context_ref = replace_url(context_ref)
                            name_tag = replace_url(name_tag)
                            name_taxonomy = replace_url(name_taxonomy)
                            data_package = {"value":data,"taxonomy":name_taxonomy}
                            if filing_dict[name_tag] == {}:
                                filing_dict[name_tag][context_ref]= data_package
                            else:
                                result = filing_dict[name_tag][context_ref]
                                if result == {}:
                                    filing_dict[name_tag][context_ref]=data_package
                                else:
                                    if type(result)==list:
                                        filing_dict[name_tag][context_ref]=result.append(data_package)
                                    else:
                                        if result == data_package:
                                            pass
                                        else:
                                            filing_dict[name_tag][context_ref]=[result,data_package] 


                            '''
                            if dict(filing_dict[context_ref][name_taxonomy])=={}:
                                filing_dict[context_ref][name_taxonomy].setdefault(name_tag, data)
                            else:

                                result = filing_dict[context_ref][name_taxonomy][name_tag]
                                if result == {}:
                                    filing_dict[context_ref][name_taxonomy][name_tag] =data
                                else:
                                    if type(result)==list:
                                        filing_dict[context_ref][name_taxonomy].setdefault(name_tag, []).append(data)
                                    else:
                                        if result == data:
                                            pass
                                        else:
                                            filing_dict[context_ref][name_taxonomy][name_tag]=[result]
                                            filing_dict[context_ref][name_taxonomy].setdefault(name_tag, []).append(data)'''
                                                    
        elif file_type == 'xml':
            for ns in ns_file:
                for el in tree.iter("{"+ns_file[ns]+"}*"):
                    attribute = dict(el.attrib)
                    if el.text != None:
                        if el.text.strip().rstrip()!="":
                            try:
                                context_ref = attribute['contextRef']
                            except KeyError:
                                pass
                            name_taxonomy = ns_file[ns]
                            name_tag = etree.QName(el).localname
                            text = format(el.text.strip().rstrip())
                            data = infer_type(text)
                            name_tag = replace_url(name_tag)
                            
                            context_ref = replace_url(context_ref)
                            name_tag = replace_url(name_tag)
                            name_taxonomy = replace_url(name_taxonomy)
                            
                            data_package = {"value":data,"taxonomy":name_taxonomy}
                            if filing_dict[name_tag] == {}:
                                filing_dict[name_tag][context_ref]= data_package
                            else:
                                result = filing_dict[name_tag][context_ref]
                                if result == {}:
                                    filing_dict[name_tag][context_ref]=data_package
                                else:
                                    if type(result)==list:
                                        filing_dict[name_tag][context_ref]=result.append(data_package)
                                    else:
                                        if result ==data_package:
                                            pass
                                        else:
                                            filing_dict[name_tag][context_ref]=[result,data_package] 

 
                            '''if dict(filing_dict[context_ref][name_taxonomy])=={}:
                                filing_dict[context_ref][name_taxonomy].setdefault(name_tag, data)
                            else:

                                result = filing_dict[context_ref][name_taxonomy][name_tag]
                                if result == {}:
                                    filing_dict[context_ref][name_taxonomy][name_tag] =data
                                else:
                                    if type(result)==list:
                                        filing_dict[context_ref][name_taxonomy].setdefault(name_tag, []).append(data)
                                    else:
                                        if result == data:
                                            pass
                                        else:
                                            filing_dict[context_ref][name_taxonomy][name_tag]=[result]
                                            filing_dict[context_ref][name_taxonomy].setdefault(name_tag, []).append(data)
'''
        filing = Filing(ch_number = ch_number, year_e = year_e)
        for key in filing_dict.keys():
            for key_ in filing_dict[key]:
                try:
                    filing_dict[key][key_]= dict(filing_dict[key][key_])
                except TypeError:
                    pass
            filing[key]=dict(filing_dict[key])
        # filing = Filing(ch_number = ch_number, year_e = year_e, ar = filing_dict).save()
        filing.save()

                