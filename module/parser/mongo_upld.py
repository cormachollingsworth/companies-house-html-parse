import json
from parser import parse
from lxml import etree
from datetime import datetime

from mongoengine import *

from .parse import get_tree


format_transformation = {"http://www.xbrl.org/inlineXBRL/transformation/2010-04-20":{"dateshortmonthyear":{"description":"Reformats date in format \"Mon (YY)YY\" into W3C/ISO date standard YYYY-MM ", "output":"datetimefield", "type":"date"}
,"datelongmonthyear" :{"description":" Reformats date in format \"Month (YY)YY\" into W3C/ISO date standard YYYY-MM","output":"datetimefield", "type":"date"}
,"dateshortyearmonth" :{"description":" Reformats date in format \"(YY)YY Mon\" into W3C/ISO date standard YYYY-MM","output":"datetimefield", "type":"date"}
,"datelongyearmonth" :{"description":" Reformats date in format \"(YY)YY Month\" into W3C/ISO date standard YYYY-MM","output":"datetimefield", "type":"date"}
,"dateslashmonthdayus" :{"description":"Reformats recurring date \"MM/DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"dateslashdaymontheu" :{"description":" Reformats recurring date \"DD/MM\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"dateshortmonthdayus" :{"description":"Reformats recurring date \"Mon DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"dateshortdaymonthuk" :{"description":" Reformats recurring date \"DD Mon\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"datelongmonthdayus" :{"description":"Reformats recurring date in format \"Month DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"datelongdaymonthuk" :{"description":" Reformats recurring date in format \"DD Month\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield", "type":"date"}
,"numspacedot" :{"description":"Reformats \"human readable\" numbers using space (" ") as a thousands separator into XSD format floating point value","output":"floatfield", "type":"float"}
,"numspacecomma" :{"description":"Reformats \"human readable\" numbers using space (" ") as a thousands separator into XSD format floating point value","output":"floatfield", "type":"float"}
,"numdotcomma" :{"description":"Reformats \"human readable\" numbers using dot (.) as a thousands separator and comma (,) as fraction separator into XSD format floating point value","output":"floatfield", "type":"float"}
,"numdash" :{"description":"Reformats accountant-friendly \"-\" as a zero.","output":"floatfield", "type":"float"}
,"numcommadot" :{"description":" Reformats \"human readable\" numbers using commas (,) as a thousands separator into XSD format floating point value","output":"floatfield", "type":"float"}
,"numcomma" :{"description":"Reformats \"human readable\" numbers using comma (,) as fraction separator into XSD format floating point value.", "output":"floatfield", "type":"float"}
,"dateslashus" :{"description":" Reformats US-style slash-separated dates into XSD format","output":"datetimefield", "type":"date"}
,"dateslasheu" :{"description":"Reformats EU-style slash-separated dates into XSD format","output":"datetimefield", "type":"date"}
,"dateshortus" :{"description":" Reformats US-style short dates into XSD format ","output":"datetimefield", "type":"date"}
,"dateshortuk" :{"description":" Reformats UK-style short dates into XSD format","output":"datetimefield", "type":"date"}
,"datelongus" :{"description":"Reformats US-style long dates into XSD format ","output":"datetimefield", "type":"date"}
,"datelonguk" :{"description":" Reformats UK-style long dates into XSD format","output":"datetimefield", "type":"date"}
,"datedotus" :{"description":" Reformats US-style dot-separated dates into XSD format ","output":"datetimefield", "type":"date"}
,"datedoteu" :{"description":" Reformats EU-style dot-separated dates into XSD format ","output":"datetimefield", "type":"date"}
}
,"http://www.xbrl.org/inlineXBRL/transformation/2011-07-31":{"booleanfalse":{"output":False,"type":"boolean"}
,"booleantrue": {"output":True, "type":"boolean"}
, "datedaymonth":{"description":"Numeric date recurring day and month", "output":"xs:gMonthDay", "exampe":"--MM-DD", "type":"date"}
,"datedaymonthen":{	"description":"English date recurring day and month" , "output":'xs:gMonthDay', "exampe":"--MM-DD", "type":"date"}
,"datedaymonthyear": {"description":"Numeric date day month and year" ,"output":"xs:date" ,"example":"YYYY-MM-DD", "type":"date"}
,"datedaymonthyearen":	{"description":"English date day month and year" ,"output": "xs:date" ,"example": "YYYY-MM-DD", "type":"date"}
,"dateerayearmonthdayjp":	{"description":"Japanese traditional date year month and day" 	, "output": 	"xs:date"	, "exampe":"YYYY-MM-DD", "type":"date"}
,"dateerayearmonthjp" :	{"description":"Japanese traditional date year and month" 	, "output": 	"xs:gYearMonth" , "exampe":		"YYYY-MM", "type":"date"}
,"datemonthday" :	{"description":"Numeric date recurring month and day" 	, "output": 	"xs:gMonthDay" 	, "exampe": 	"--MM-DD", "type":"date"}
,"datemonthdayen" :	{"description":"English date recurring month and day" 	, "output": 	"xs:gMonthDay" , "exampe":	"--MM-DD", "type":"date"}
,"datemonthdayyear" :	{"description":"Numeric date month day and year" 	, "output": 	"xs:date" 	 , "exampe":	"YYYY-MM-DD", "type":"date"}
,"datemonthdayyearen" :	{"description":"English date month day and year" 	, "output": 	"xs:date" 	, "exampe": 	"YYYY-MM-DD", "type":"date"}
,"datemonthyearen" :	{"description":"English date month and year" 	, "output":	"xs:gYearMonth" 	, "exampe": 	"YYYY-MM", "type":"date"}
,"dateyearmonthdaycjk" :	{"description":"Japanese/Chinese/Korean date year month and day" 	, "output": 	"xs:date" 	 , "exampe":	"YYYY-MM-DD", "type":"date"}
,"dateyearmonthen" :	{"description":"English date year and month"	, "output": 	"xs:gYearMonth" 	 , "exampe":	"YYYY-MM", "type":"date"}
,"dateyearmonthcjk" :	{"description":"Japanese/Chinese/Korean date year and month", "output":	"xs:gYearMonth" 	 , "exampe":	"YYYY-MM", "type":"date"}
,"nocontent" :	{"description":"Any string" 	, "output": 	"ixt:nocontentType" , "exampe":	"*", , "type":"null" 	}
,"numcommadecimal" :	{"description":"Number with comma fraction separator" 	 , "output":	"ixt:nonNegativeDecimalType" 	 , "exampe":	"nnnnnnnnn.n", "type":"float"}
,"numdotdecimal" :	{"description":"Number with dot fraction separator" , "output":		"ixt:nonNegativeDecimalType" , "exampe":	 	"nnnnnnnnn.n","type":"float"}
,"numunitdecimal" :	{"description":"Number with unit strings" 	, "output":	"ixt:nonNegativeDecimalType" 	 , "exampe":	"nnnnnnnnn.n","type":"float"}
,"zerodash" :	{"description":"Zero dash"  "0","type":"dashzero"}
},
'http://www.xbrl.org/2008/inlineXBRL/transformation':{"dateshortmonthyear":{"description":"Reformats date in format \"Mon (YY)YY\" into W3C/ISO date standard YYYY-MM ", "output":"datetimefield","type":"date"}
,"datelongmonthyear" :{"description":" Reformats date in format \"Month (YY)YY\" into W3C/ISO date standard YYYY-MM","output":"datetimefield","type":"date"}
,"dateshortyearmonth" :{"description":" Reformats date in format \"(YY)YY Mon\" into W3C/ISO date standard YYYY-MM","output":"datetimefield","type":"date"}
,"datelongyearmonth" :{"description":" Reformats date in format \"(YY)YY Month\" into W3C/ISO date standard YYYY-MM","output":"datetimefield","type":"date"}
,"dateslashmonthdayus" :{"description":"Reformats recurring date \"MM/DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"dateslashdaymontheu" :{"description":" Reformats recurring date \"DD/MM\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"dateshortmonthdayus" :{"description":"Reformats recurring date \"Mon DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"dateshortdaymonthuk" :{"description":" Reformats recurring date \"DD Mon\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"datelongmonthdayus" :{"description":"Reformats recurring date in format \"Month DD\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"datelongdaymonthuk" :{"description":" Reformats recurring date in format \"DD Month\" into W3C/ISO recurring date standard --MM-DD","output":"datetimefield","type":"date"}
,"numspacedot" :{"description":"Reformats \"human readable\" numbers using space (" ") as a thousands separator into XSD format floating point value","output":"floatfield","type":"float"}
,"numspacecomma" :{"description":"Reformats \"human readable\" numbers using space (" ") as a thousands separator into XSD format floating point value","output":"floatfield","type":"float"}
,"numdotcomma" :{"description":"Reformats \"human readable\" numbers using dot (.) as a thousands separator and comma (,) as fraction separator into XSD format floating point value","output":"floatfield","type":"float"}
,"numdash" :{"description":"Reformats accountant-friendly \"-\" as a zero.","output":"floatfield","type":"float"}
,"numcommadot" :{"description":" Reformats \"human readable\" numbers using commas (,) as a thousands separator into XSD format floating point value","output":"floatfield","type":"float"}
,"numcomma" :{"description":"Reformats \"human readable\" numbers using comma (,) as fraction separator into XSD format floating point value.", "output":"floatfield","type":"float"}
,"dateslashus" :{"description":" Reformats US-style slash-separated dates into XSD format","output":"datetimefield","type":"date"}
,"dateslasheu" :{"description":"Reformats EU-style slash-separated dates into XSD format","output":"datetimefield","type":"float"}
,"dateshortus" :{"description":" Reformats US-style short dates into XSD format ","output":"datetimefield","type":"float"}
,"dateshortuk" :{"description":" Reformats UK-style short dates into XSD format","output":"datetimefield","type":"float"}
,"datelongus" :{"description":"Reformats US-style long dates into XSD format ","output":"datetimefield","type":"float"}
,"datelonguk" :{"description":" Reformats UK-style long dates into XSD format","output":"datetimefield","type":"float"}
,"datedotus" :{"description":" Reformats US-style dot-separated dates into XSD format ","output":"datetimefield","type":"float"}
,"datedoteu" :{"description":" Reformats EU-style dot-separated dates into XSD format ","output":"datetimefield","type":"float"}
}
}

def transform_data(data_format, data):
    '''
    https://specifications.xbrl.org/release-history-transformation-registry-1-trr1.html

    https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/366239/additional-guidance.pdf
    
    version 1 link is broken:
    http://www.xbrl.org/specification/inlinexbrl-specifiedtransformations/rec-2010-04-20/inlinexbrl-specifiedtransformations-rec-2010-04-20.html
    
    ixt: and ixt1: for Version 1 and ixt2: for Version 2
    also ixt1 is used too!
    
    ixt2: http://www.xbrl.org/specification/inlinexbrl-transformationregistry/rec-2011-07-31/inlinexbrl-transformationregistry-rec-2011-07-31.html#sec-ixt-18
    
    'ixt': ['http://www.xbrl.org/inlineXBRL/transformation/2010-04-20',
    'http://www.xbrl.org/inlineXBRL/transformation/2011-07-31',
    'http://www.xbrl.org/2008/inlineXBRL/transformation']
    'ixt2': ['http://www.xbrl.org/inlineXBRL/transformation/2011-07-31']
    'ixt1': ['http://www.xbrl.org/inlineXBRL/transformation/2010-04-20']
    
    '''

    if data_format == 'ixt2:booleanfalse':
        return data=='true' #actually this is false!
    if data_format =='ixt2:datedaymonthyear':
        return pd.to_datetime(data).to_pydatetime().strftime("%d/%m/%Y")
    if data_format=='ixt2:datedaymonthyearen':
        return pd.to_datetime(data).to_pydatetime().strftime("%d/%m/%Y")
    #TODO: change the dates into strings - but the same formate
    if data_format =='ixt2:numdotdecimal':
        return float(data.replace(',',''))
    if data_format == 'ixt:numcommadot':
        return int(data)

# TODO: remove the text logic
# TODO: need to go into the keys of the namespace to access what the ixt, ixt1, ixt2 are
# TODO: the namespace will direct to a particular web address

def add_element(mongo_doc, element):
    '''
    https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/366239/additional-guidance.pdf
    
    version 1 link is broken:
    http://www.xbrl.org/specification/inlinexbrl-specifiedtransformations/rec-2010-04-20/inlinexbrl-specifiedtransformations-rec-2010-04-20.html
    
    ixt: and ixt1: for Version 1 and ixt2: for Version 2
    also ixt1 is used too!
    
    ixt2: http://www.xbrl.org/specification/inlinexbrl-transformationregistry/rec-2011-07-31/inlinexbrl-transformationregistry-rec-2011-07-31.html#sec-ixt-18
    
    Note: the first part is the key for the namespace link.

    'ixt': ['http://www.xbrl.org/inlineXBRL/transformation/2010-04-20',
    'http://www.xbrl.org/inlineXBRL/transformation/2011-07-31',
    'http://www.xbrl.org/2008/inlineXBRL/transformation']
    'ixt2': ['http://www.xbrl.org/inlineXBRL/transformation/2011-07-31'],
    'ixt1': ['http://www.xbrl.org/inlineXBRL/transformation/2010-04-20'],

    
    '''
    attribute = dict(element.attrib)
    if attribute != {}:
        value = element.text
        if 'format' in attribute.keys():
            format_=attribute['format'].split(':')
            # TODO: use the namespace to lookup the namespaces as per comment
            if format_[0]=='ixt2':
                data_type=attribute['name'].split(':')[1]
            if format_[0]== 'ixt2:nocontent':
                if data_type not in x.keys():
                    x[data_type]=None
            else:
                data_format=attribute['format']
                value = format_data(data_format, el.text)
                if 'unitRef' in attribute.keys():
                    value = {'Amount':value, 'Currency':attribute['unitRef']}
                if data_type not in x.keys():
                    x[data_type]=value

        else:
            data_type=attribute['name'].split(':')[1]
            if data_type not in x.keys():
                x[data_type]=value

class Company(Document):
    ch_num = IntField(min_value=0, required=True)

class 

class Filing(DynamicDocument):
    ch_num = IntField(min_value=0, required=True)
    year_end = DateTimeField()



if __name__ == "__main__":

    file_names = os.listdir('./annual_reports')
    file_name = file_names[45]
    extract = file_name.split("_")
    ch_number = extract[2]
    year_e = datetime.strptime(re.match(r'^(.*?)\..*',extract[3])[1],"%Y%m%d")

    inline_xbrl_keys = ['ixt','ixt1','ixt2']
    
    file_type = file_name.split('.')[-1]
    if file_type == 'html':
        tree = get_tree(file_name)
        ns_file = parse.read_ns(ch_file)

        for xbrl_key in inline_xbrl_keys:
            if xbrl_key in ns_file.keys():

    f = Filing(ch_num = ch_number, year_end = year_e)