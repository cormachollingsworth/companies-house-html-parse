def find_attrib(attribute, file_name):
    tree = get_tree(file_name)
    path = ".//*[@{}]".format(attribute)
    g = tree.iterfind(path, namespaces=tree.nsmap)
    try:
        next(g).attrib
        return 0.
    except StopIteration:
        return 1.

def find_attrib_name(attribute, file_name):
    tree = get_tree(file_name)
    path = ".//*[@{}]".format(attribute)
    g = tree.iterfind(path, namespaces=tree.nsmap)
    try:
        next(g).attrib
    except StopIteration:
        return file_name

for file_name in file_names:
    x+=find_attrib('name', file_name)

xml_files = []
for file_name in file_names:
    name = find_attrib('name', file_name)
    if name:
        xml_files.append(name)

## Only the xml files are built differently - explore the XML!

xml_files[4]

tree = get_tree(xml_files[4])

xml_ns_map = tree.nsmap

for ns in xml_ns_map:
    print(xml_ns_map[ns])
    for el in tree.iter("{"+xml_ns_map[ns]+"}*"):
        attribute = dict(el.attrib)
        if el.text != None:
            if el.text.strip().rstrip()!="":
                print(el.attrib,etree.QName(el).localname, "{}".format(el.text.strip().rstrip()))

for ns in xml_ns_map:
    print(xml_ns_map[ns])
    for el in tree.iter("{"+xml_ns_map[ns]+"}*"):


for ns in ns_file:
    print("{"+ns_file[ns]+"}")