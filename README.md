# Companies House HTML Parse

`upload_taxonomy.py` loads from an unzipped set of daily accounts posted at companies house
The taxonomy is in `ar_class.py` and this is created in the `create_taxonomy.py` file
To ensure names are pythonic, `make_python_identifier.py` is used

Parsing ixbrl in the HTML files from Companies House

<p>http://download.companieshouse.gov.uk/en_monthlyaccountsdata.html</p>
<p>http://download.companieshouse.gov.uk/en_accountsdata.html</p>
<p>http://download.companieshouse.gov.uk/en_output.html</p>

Jupyter notebook parts of this:

https://blog.ouseful.info/2019/02/05/on-not-faffing-around-with-jupyter-docker-container-auth-tokens/

-e JUPYTER_TOKEN="jupyter_notebook_token"

docker build --tag cmdstan .
docker run -e JUPYTER_TOKEN="jupyter_notebook_token" -dp 8888:8888 cmdstan