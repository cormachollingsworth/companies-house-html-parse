FROM python:3.7.3-stretch as base
FROM base as builder
RUN mkdir /install
WORKDIR /install
COPY requirements.txt /requirements.txt
RUN pip install --install-option="--prefix=/install" -r /requirements.txt

FROM base
COPY --from=builder /install /usr/local
COPY module/parse.py /
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser