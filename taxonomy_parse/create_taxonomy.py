from datetime import datetime
from dateutil.parser import parse
from dateutil.parser._parser import ParserError
import calendar
from distutils.util import strtobool
from collections import defaultdict

import pandas as pd

from make_python_identifier import make_python_identifier

def assertion_applied(text, row):
    assert row[text]==row[f"{text}_x"]==row[f"{text}_y"]


file_102='FRC-2019-Taxonomy-v1.1.0-FRS102.xls'
file_IFRS = 'FRC-2019-Taxonomy-v1.1.0-IFRS.xls'
file_101 = 'FRC-2019-Taxonomy-v1.1.0-FRS101.xls'

'''Presentation
Hypercube items
Hypercube dimensions
Dimension members
Cross-references
Inflows
Outflows
'''

wb_102 = pd.read_excel(file_102, sheet_name = 'Presentation')
wb_101 = pd.read_excel(file_101, sheet_name='Presentation')
wb_ifrs = pd.read_excel(file_IFRS, sheet_name='Presentation')

wb_102 = wb_102.loc[wb_102.Name.notna()].drop_duplicates()
wb_101 = wb_101.loc[wb_101.Name.notna()].drop_duplicates()
wb_ifrs = wb_ifrs.loc[wb_ifrs.Name.notna()].drop_duplicates()

intermediate = pd.merge(wb_101, wb_ifrs, how='outer', on='Name')
wb_full = pd.merge(intermediate,wb_102, how='outer',on='Name').reset_index()

wb_full = wb_full[wb_full['Abstract']==False].copy()

def infer_type(text):
    try:
        data = float(text)
    except ValueError:
        try:
            data = parse(text)
        except (ParserError, calendar.IllegalMonthError, TypeError):
            try:
                data = strtobool(text)
                data = bool(data)
            except ValueError:
                data = str(text)
    return data

text = '''from urllib.parse import urlparse
import calendar
from dateutil.parser import parse
from dateutil.parser._parser import ParserError
from distutils.util import strtobool
from mongoengine import Document, EmbeddedDocument, DynamicDocument
from mongoengine.errors import ValidationError
from mongoengine.fields import (StringField, DateTimeField, EmbeddedDocumentField, IntField, URLField)

class Annual_Report(EmbeddedDocument):
    format=StringField()
    decimal=IntField()
    context_ref=StringField()
    id=StringField()
    value=StringField()
    taxonomy=URLField()
    regulation_text = StringField()
    prefix = StringField()

class Filing(DynamicDocument):
    meta = {'collection': 'ch_filing'} 
    ch_number = StringField(required=True)
    year_e = DateTimeField(required=True)
'''
dict_text = '''
meta_data = {\n'''

for index, row in wb_full.iterrows():
    field_name = row['Name']
    if field_name.isidentifier():
        pass
    else:
        field_name, _ = make_python_identifier(field_name)

    #TODO: validate that the name is pythonic 
    # https://stackoverflow.com/questions/36330860/pythonically-check-if-a-variable-name-is-valid
    field_type = row['Type']

    FRS101 = row['FRS 101']
    FRS102 = row['FRS 102']
    Full = row['Full']
    Full_IFRS = row['Full / FRS101_x']
    if pd.isna(Full_IFRS):
        Full_IFRS = row['Full / FRS101_y']

    CA = row['Companies Act']
    if pd.isna(CA):
        CA = row['Companies Act_x']
        if pd.isna(CA):
            CA = row['Companies Act_y']
    AReg = row['Audit Regulations']

    attribute = row['Prefix']
    description = row['Label']

    if pd.isna(attribute):
        attribute = row['Prefix_x']
        if pd.isna(attribute):
            attribute = row['Prefix_y']

    if pd.isna(FRS101):
        
        if pd.isna(CA):
            pass
        else:
            description+=f'{CA}'

        if pd.isna(AReg):
            pass
        else:
            assertion_applied('Audit Regulations', row)
            description += f'{AReg}'

        if pd.isna(Full):
            pass
        else:
            description += f'{Full}'
        
        if pd.isna(Full_IFRS):
            pass
        else:
            description+=f'{Full_IFRS}'


    else:

        if pd.isna(CA):
            pass
        else:
            description+=f'{CA}'

        if pd.isna(AReg):
            pass
        else:
            assertion_applied('Audit Regulations', row)
            description += f'{AReg}'


        if pd.isna(Full):
            pass
        else:
            description += f'{Full}'
        description+=f'{FRS101}'
        description+=f'{FRS102}'

    text+= f'    {field_name} = EmbeddedDocumentField(Annual_Report)\n'
    dict_text+=f'    \"{field_name}\":{{"regulation_text" : "{description}", "prefix":"{attribute}"}},'

dict_text+="}"
text+=dict_text
with open('../module/ar_class.py', 'w') as f:
    f.write(text)

